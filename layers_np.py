# -*- coding: utf-8 -*-
"""layers_np.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1s_reY4Ru6XVDJRKEo-PPqPlMztzCRNeA
"""

import numpy as np
import scipy.special

class Linear():
    """
    A module which applies a linear transformation 
    A common name is fully-connected layer, InnerProductLayer in caffe. 
    
    The module should work with 2D input of shape (n_samples, n_feature).
    """
    def __init__(self, n_in, n_out):
       
        self.output = None
        self.gradInput = None
        self.training = True

        stdv = 1./np.sqrt(n_in)
        self.W = np.random.uniform(-stdv, stdv, size = (n_out, n_in))
        self.b = np.random.uniform(-stdv, stdv, size = n_out)
        
        self.gradW = np.zeros_like(self.W)
        self.gradb = np.zeros_like(self.b)
        
    def forward(self, input):
        self.output = np.add(np.matmul(input, self.W.T),self.b) 
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.matmul(gradOutput, self.W)
        # Accelerate grad parameters
        self.gradW = np.matmul(gradOutput.T, input)
        self.gradb = gradOutput.sum(axis=0)
        return self.gradInput
    
    def zeroGradParameters(self):
        self.gradW.fill(0)
        self.gradb.fill(0)
        
    def getParameters(self):
        return [self.W, self.b]
    
    def getGradParameters(self):
        return [self.gradW, self.gradb]


class SoftMax():
    def __init__(self):
      
        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        # start with normalization for numerical stability
        input = np.subtract(input , np.expand_dims(np.max(input, axis=-1),axis=-1))
        output = np.divide(np.exp(input), np.expand_dims(np.sum(np.exp(input), axis=1),axis=-1))
        return output
    
    def backward(self, input, gradOutput):
        S = self.forward(input)
        S_vector = S.reshape(S.shape[0],S.shape[1],1)
        S_matrix = np.tile(S_vector,S.shape[1])
        J = np.subtract(np.multiply(np.eye(S.shape[1],S.shape[1]),S_vector),np.multiply(S_matrix.transpose(0,2,1), S_matrix))
        self.gradInput = np.squeeze(np.matmul(J,np.expand_dims(gradOutput,axis=-1)),axis=-1)
        return self.gradInput

    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass

class LogSoftMax():
    def __init__(self):
        
        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        # start with normalization for numerical stability
        input = np.subtract(input , np.expand_dims(np.max(input, axis=-1),axis=-1))
        self.output = np.subtract(input, np.expand_dims(np.log(np.sum(np.exp(input), axis=1)),axis=-1)) 
        return self.output
    
    def backward(self, input, gradOutput):
        softmax = SoftMax()
        I_3d = np.repeat(np.eye(input.shape[1],input.shape[1])[np.newaxis,:, :], input.shape[0], axis=0)
        S = softmax.forward(input)
        S_vector = S.reshape(S.shape[0],S.shape[1],1)
        S_matrix = np.tile(S_vector,S.shape[1])
        J = np.subtract(I_3d, S_matrix)
        self.gradInput = np.squeeze(np.matmul(J,np.expand_dims(gradOutput,axis=-1)), axis=-1)
        return self.gradInput

    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []

    def zeroGradParameters(self): 
        pass

class BatchNormalization:
    EPS = 1e-3
    
    def __init__(self, alpha = 0.):

        self.output = None
        self.gradInput = None
        self.training = True

        self.alpha = alpha
        self.moving_mean = None 
        self.moving_variance = None
        
    def forward(self, input):         
        if self.training:
            batch_mean = np.mean(input, axis=0, keepdims=True)
            centered_input = input - batch_mean
            batch_variance = np.var(input, axis=0, keepdims=True)
            
            if self.moving_mean is None:
                self.moving_mean = batch_mean
            else:
                self.moving_mean = self.moving_mean * self.alpha + batch_mean * (1 - self.alpha)
            
            if self.moving_variance is None:
                self.moving_variance = batch_variance
            else:
                self.moving_variance = self.moving_variance * self.alpha + batch_variance * (1 - self.alpha)
            
            self.output = centered_input / np.sqrt(batch_variance + self.EPS)
        else:
            if self.moving_mean is None:
                self.output = input
            else:
                self.output = (input - self.moving_mean) / np.sqrt(self.moving_variance + self.EPS)
            
        return self.output
    
    def backward(self, input, gradOutput):
        m = input.shape[0]
        batch_variance = np.var(input, axis=0, keepdims=True)
        N = np.divide(np.power(np.add(batch_variance,self.EPS),-0.5), m)
        d1 = np.add(np.sum(gradOutput, axis=0), np.multiply(self.output, np.sum(np.multiply(gradOutput,self.output), axis=0)))
        d2 = np.subtract(np.multiply(m, gradOutput), d1)
        self.gradInput = np.multiply(N, d2)
        return self.gradInput
        
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass

class ChannelwiseScaling:
    def __init__(self, n_out):

        self.output = None
        self.gradInput = None
        self.training = True

        stdv = 1./np.sqrt(n_out)
        self.gamma = np.random.uniform(-stdv, stdv, size=n_out)
        self.beta = np.random.uniform(-stdv, stdv, size=n_out)
        
        self.gradGamma = np.zeros_like(self.gamma)
        self.gradBeta = np.zeros_like(self.beta)

    def forward(self, input):
        self.output = input * self.gamma + self.beta
        return self.output
        
    def backward(self, input, gradOutput):
        self.gradInput = gradOutput * self.gamma
        # Accelerate grad parameters
        self.gradBeta = np.sum(gradOutput, axis=0)
        self.gradGamma = np.sum(gradOutput*input, axis=0)
        return self.gradInput
    
    def zeroGradParameters(self):
        self.gradGamma.fill(0)
        self.gradBeta.fill(0)
        
    def getParameters(self):
        return [self.gamma, self.beta]
    
    def getGradParameters(self):
        return [self.gradGamma, self.gradBeta]
    
    def zeroGradParameters(self): 
        pass    

class Dropout:
    def __init__(self, p=0.5):

        self.output = None
        self.gradInput = None
        self.training = True
        
        self.p = p
        self.mask = None
        
    def forward(self, input):
        if self.training:
            self.mask = np.random.binomial(1, p=1-self.p, size=input.shape)
            self.output = np.multiply(np.multiply(input, self.mask), 1/(1-self.p))
        else:
            self.output = input
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.multiply(np.multiply(self.mask, gradOutput), 1/(1-self.p))
        return self.gradInput 
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass  

class ReLU():
    def __init__(self):
      
        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        self.output = np.maximum(input, 0)
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.multiply(gradOutput, input > 0)
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass  

class LeakyReLU():
    def __init__(self, slope = 0.03):
        
        self.output = None
        self.gradInput = None
        self.training = True
            
        self.slope = slope
        
    def forward(self, input):
        self.output = np.maximum(input, np.multiply(input, self.slope))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.add(np.multiply(gradOutput, input > 0), np.multiply(np.multiply(gradOutput,self.slope), input <= 0))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class ELU():
    def __init__(self, alpha = 1.0):
        self.output = None
        self.gradInput = None
        self.training = True
        
        self.alpha = alpha
        
    def forward(self, input):
        self.output = np.add(np.multiply(input, input > 0), np.multiply(np.multiply(self.alpha,np.subtract(np.exp(input),1)), input <= 0))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.add(np.multiply(gradOutput, input > 0), np.multiply(np.multiply(gradOutput,np.multiply(self.alpha,np.exp(input))), input <= 0))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 


class SoftPlus():
    def __init__(self):

        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        self.output = np.log(np.add(1, np.exp(input)))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = np.multiply(gradOutput, np.divide(1,np.add(1,np.exp(-input))))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class MSECriterion:
    def __init__(self):
        self.output = None
        self.gradInput = None
        
    def forward(self, input, target):   
        self.output = np.sum(np.power(input - target,2)) / input.shape[0]
        return self.output 
 
    def backward(self, input, target):
        self.gradInput  = (input - target) * 2 / input.shape[0]
        return self.gradInput

class ClassNLLCriterion:
    def __init__(self):
        self.output = None
        self.gradInput = None
        
    def forward(self, input, target): 
        self.output = -np.divide(np.sum(np.multiply(input, target)), input.shape[0])
        return self.output

    def backward(self, input, target):
        self.gradInput = -np.divide(target, input.shape[0])
        return self.gradInput

def sgd_momentum(variables, gradients, config, state):  
    state.setdefault('accumulated_grads', {})
    
    var_index = 0 
    for current_layer_vars, current_layer_grads in zip(variables, gradients): 
        for current_var, current_grad in zip(current_layer_vars, current_layer_grads):
            
            old_grad = state['accumulated_grads'].setdefault(var_index, np.zeros_like(current_grad))
            
            np.add(config['momentum'] * old_grad, config['learning_rate'] * current_grad, out=old_grad)
            
            current_var -= old_grad
            var_index += 1     

def adam_optimizer(variables, gradients, config, state):  
    state.setdefault('m', {})  # first moment vars
    state.setdefault('v', {})  # second moment vars
    state.setdefault('t', 0)   # timestamp
    state['t'] += 1
    for k in ['learning_rate', 'beta1', 'beta2', 'epsilon']:
        assert k in config, config.keys()
    
    var_index = 0 
    lr_t = config['learning_rate'] * np.sqrt(1 - config['beta2']**state['t']) / (1 - config['beta1']**state['t'])
    for current_layer_vars, current_layer_grads in zip(variables, gradients): 
        for current_var, current_grad in zip(current_layer_vars, current_layer_grads):
            var_first_moment = state['m'].setdefault(var_index, np.zeros_like(current_grad))
            var_second_moment = state['v'].setdefault(var_index, np.zeros_like(current_grad))
            np.add(np.multiply(config['beta1'],var_first_moment),np.multiply(np.subtract(1,config['beta1']),current_grad), out=var_first_moment)
            np.add(np.multiply(config['beta2'],var_second_moment),np.multiply(np.subtract(1,config['beta2']),np.multiply(current_grad,current_grad)), out=var_second_moment)
            current_var -= np.divide(np.multiply(lr_t,var_first_moment),np.add(config['epsilon'], np.sqrt(var_second_moment)))
            var_index += 1

