# -*- coding: utf-8 -*-
"""layers_cp.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1aPKpyjQXAp4tRuVKTUjc83FyMvcy1PE2
"""

import cupy as cp
import scipy.special

class cp_Linear():
    def __init__(self, n_in, n_out):
       
        self.output = None
        self.gradInput = None
        self.training = True

        stdv = 1./cp.sqrt(n_in)
        self.W = cp.random.uniform(-stdv, stdv, size = (n_out, n_in))
        self.b = cp.random.uniform(-stdv, stdv, size = n_out)
        
        self.gradW = cp.zeros_like(self.W)
        self.gradb = cp.zeros_like(self.b)
        
    def forward(self, input):
        self.output = cp.add(cp.matmul(input, self.W.T),self.b) 
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.matmul(gradOutput, self.W)
        # Accelerate grad parameters
        self.gradW = cp.matmul(gradOutput.T, input)
        self.gradb = gradOutput.sum(axis=0)
        return self.gradInput
    
    def zeroGradParameters(self):
        self.gradW.fill(0)
        self.gradb.fill(0)
        
    def getParameters(self):
        return [self.W, self.b]
    
    def getGradParameters(self):
        return [self.gradW, self.gradb]
    
    def zeroGradParameters(self): 
        pass 

class cp_SoftMax():
    def __init__(self):
      
        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        # start with normalization for numerical stability
        input = cp.subtract(input , cp.expand_dims(cp.max(input, axis=-1),axis=-1))
        output = cp.divide(cp.exp(input), cp.expand_dims(cp.sum(cp.exp(input), axis=1),axis=-1))
        return output
    
    def backward(self, input, gradOutput):
        S = self.forward(input)
        S_vector = S.reshape(S.shape[0],S.shape[1],1)
        S_matrix = cp.tile(S_vector,S.shape[1])
        J = cp.subtract(cp.multiply(cp.eye(S.shape[1],S.shape[1]),S_vector),cp.multiply(S_matrix.transpose(0,2,1), S_matrix))
        self.gradInput = cp.squeeze(cp.matmul(J,cp.expand_dims(gradOutput,axis=-1)),axis=-1)
        return self.gradInput

    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class cp_LogSoftMax():
    def __init__(self):
        
        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        # start with normalization for numerical stability
        input = cp.subtract(input , cp.expand_dims(cp.max(input, axis=-1),axis=-1))
        self.output = cp.subtract(input, cp.expand_dims(cp.log(cp.sum(cp.exp(input), axis=1)),axis=-1)) 
        return self.output
    
    def backward(self, input, gradOutput):
        softmax = cp_SoftMax()
        I_3d = cp.repeat(cp.eye(input.shape[1],input.shape[1])[cp.newaxis,:, :], input.shape[0], axis=0)
        S = softmax.forward(input)
        S_vector = S.reshape(S.shape[0],S.shape[1],1)
        S_matrix = cp.tile(S_vector,S.shape[1])
        J = cp.subtract(I_3d, S_matrix)
        self.gradInput = cp.squeeze(cp.matmul(J,cp.expand_dims(gradOutput,axis=-1)), axis=-1)
        return self.gradInput

    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class cp_BatchNormalization():
    EPS = 1e-3
    
    def __init__(self, alpha = 0.):

        self.output = None
        self.gradInput = None
        self.training = True

        self.alpha = alpha
        self.moving_mean = None 
        self.moving_variance = None
        
    def forward(self, input):         
        if self.training:
            batch_mean = cp.mean(input, axis=0, keepdims=True)
            centered_input = input - batch_mean
            batch_variance = cp.var(input, axis=0, keepdims=True)
            
            if self.moving_mean is None:
                self.moving_mean = batch_mean
            else:
                self.moving_mean = self.moving_mean * self.alpha + batch_mean * (1 - self.alpha)
            
            if self.moving_variance is None:
                self.moving_variance = batch_variance
            else:
                self.moving_variance = self.moving_variance * self.alpha + batch_variance * (1 - self.alpha)
            
            self.output = centered_input / cp.sqrt(batch_variance + self.EPS)
        else:
            if self.moving_mean is None:
                self.output = input
            else:
                self.output = (input - self.moving_mean) / cp.sqrt(self.moving_variance + self.EPS)
            
        return self.output
    
    def backward(self, input, gradOutput):
        m = input.shape[0]
        batch_variance = cp.var(input, axis=0, keepdims=True)
        N = cp.divide(cp.power(cp.add(batch_variance,self.EPS),-0.5), m)
        d1 = cp.add(cp.sum(gradOutput, axis=0), cp.multiply(self.output, cp.sum(cp.multiply(gradOutput,self.output), axis=0)))
        d2 = cp.subtract(cp.multiply(m, gradOutput), d1)
        self.gradInput = cp.multiply(N, d2)
        return self.gradInput

    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
        
    def zeroGradParameters(self): 
        pass 

class cp_ChannelwiseScaling():
    def __init__(self, n_out):

        self.output = None
        self.gradInput = None
        self.training = True

        stdv = 1./cp.sqrt(n_out)
        self.gamma = cp.random.uniform(-stdv, stdv, size=n_out)
        self.beta = cp.random.uniform(-stdv, stdv, size=n_out)
        
        self.gradGamma = cp.zeros_like(self.gamma)
        self.gradBeta = cp.zeros_like(self.beta)

    def forward(self, input):
        self.output = input * self.gamma + self.beta
        return self.output
        
    def backward(self, input, gradOutput):
        self.gradInput = gradOutput * self.gamma
        # Accelerate grad parameters
        self.gradBeta = cp.sum(gradOutput, axis=0)
        self.gradGamma = cp.sum(gradOutput*input, axis=0)
        return self.gradInput
    
    def zeroGradParameters(self):
        self.gradGamma.fill(0)
        self.gradBeta.fill(0)
        
    def getParameters(self):
        return [self.gamma, self.beta]
    
    def getGradParameters(self):
        return [self.gradGamma, self.gradBeta]
    
    def zeroGradParameters(self): 
        pass 

class cp_Dropout():
    def __init__(self, p=0.5):

        self.output = None
        self.gradInput = None
        self.training = True
        
        self.p = p
        self.mask = None
        
    def forward(self, input):
        if self.training:
            self.mask = cp.random.binomial(1, p=1-self.p, size=input.shape)
            self.output = cp.multiply(cp.multiply(input, self.mask), 1/(1-self.p))
        else:
            self.output = input
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.multiply(cp.multiply(self.mask, gradOutput), 1/(1-self.p))
        return self.gradInput 
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class cp_ReLU():
    def __init__(self):

        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        self.output = cp.maximum(input, 0)
        return self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.multiply(gradOutput, input > 0)
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class cp_LeakyReLU():
    def __init__(self, slope = 0.03):
        
        self.output = None
        self.gradInput = None
        self.training = True
            
        self.slope = slope
        
    def forward(self, input):
        self.output = cp.maximum(input, cp.multiply(input, self.slope))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.add(cp.multiply(gradOutput, input > 0), cp.multiply(cp.multiply(gradOutput,self.slope), input <= 0))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 


class cp_ELU():
    def __init__(self, alpha = 1.0):
        self.output = None
        self.gradInput = None
        self.training = True
        
        self.alpha = alpha
        
    def forward(self, input):
        self.output = cp.add(cp.multiply(input, input > 0), cp.multiply(cp.multiply(self.alpha,cp.subtract(cp.exp(input),1)), input <= 0))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.add(cp.multiply(gradOutput, input > 0), cp.multiply(cp.multiply(gradOutput,cp.multiply(self.alpha,cp.exp(input))), input <= 0))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 


class cp_SoftPlus():
    def __init__(self):

        self.output = None
        self.gradInput = None
        self.training = True
    
    def forward(self, input):
        self.output = cp.log(cp.add(1, cp.exp(input)))
        return  self.output
    
    def backward(self, input, gradOutput):
        self.gradInput = cp.multiply(gradOutput, cp.divide(1,cp.add(1,cp.exp(-input))))
        return self.gradInput
    
    def getParameters(self):
        return []
        
    def getGradParameters(self):
        return []
    
    def zeroGradParameters(self): 
        pass 

class cp_MSECriterion:
    def __init__(self):
        self.output = None
        self.gradInput = None
        
    def forward(self, input, target):   
        self.output = cp.sum(cp.power(input - target,2)) / input.shape[0]
        return self.output 
 
    def backward(self, input, target):
        self.gradInput  = (input - target) * 2 / input.shape[0]
        return self.gradInput

class cp_ClassNLLCriterion:
    def __init__(self):
        self.output = None
        self.gradInput = None
        
    def forward(self, input, target): 
        self.output = -cp.divide(cp.sum(cp.multiply(input, target)), input.shape[0])
        return self.output

    def backward(self, input, target):
        self.gradInput = -cp.divide(target, input.shape[0])
        return self.gradInput
      
def cp_sgd_momentum(variables, gradients, config, state):  
    state.setdefault('accumulated_grads', {})
    
    var_index = 0 
    for current_layer_vars, current_layer_grads in zip(variables, gradients): 
        for current_var, current_grad in zip(current_layer_vars, current_layer_grads):
            
            old_grad = state['accumulated_grads'].setdefault(var_index, cp.zeros_like(current_grad))
            
            cp.add(config['momentum'] * old_grad, config['learning_rate'] * current_grad, out=old_grad)
            
            current_var -= old_grad
            var_index += 1     

def cp_adam_optimizer(variables, gradients, config, state):  
  
    state.setdefault('m', {})  # first moment vars
    state.setdefault('v', {})  # second moment vars
    state.setdefault('t', 0)   # timestamp
    state['t'] += 1
    for k in ['learning_rate', 'beta1', 'beta2', 'epsilon']:
        assert k in config, config.keys()
    
    var_index = 0 
    lr_t = config['learning_rate'] * cp.sqrt(1 - config['beta2']**state['t']) / (1 - config['beta1']**state['t'])
    for current_layer_vars, current_layer_grads in zip(variables, gradients): 
        for current_var, current_grad in zip(current_layer_vars, current_layer_grads):
            var_first_moment = state['m'].setdefault(var_index, cp.zeros_like(current_grad))
            var_second_moment = state['v'].setdefault(var_index, cp.zeros_like(current_grad))
        
            cp.add(cp.multiply(config['beta1'],var_first_moment),cp.multiply(cp.subtract(1,config['beta1']),current_grad), out=var_first_moment)
            cp.add(cp.multiply(config['beta2'],var_second_moment),cp.multiply(cp.subtract(1,config['beta2']),cp.multiply(current_grad,current_grad)), out=var_second_moment)
            current_var -= cp.divide(cp.multiply(lr_t,var_first_moment),cp.add(config['epsilon'], cp.sqrt(var_second_moment)))

            assert var_first_moment is state['m'].get(var_index)
            assert var_second_moment is state['v'].get(var_index)
            var_index += 1

